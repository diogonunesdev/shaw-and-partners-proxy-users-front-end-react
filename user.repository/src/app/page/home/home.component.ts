import { formatDate } from '@angular/common';
import { AuthService } from 'src/app/service/auth.service';
import { Component, OnInit } from '@angular/core';
import { UtilService } from 'src/app/service/util.service';
import { AjaxService } from 'src/app/service/ajax.service';
import { Router } from '@angular/router';
import { ResponseUser } from 'src/app/model/User.model';
import { API } from 'src/environments/environment';
import 'sweetalert2/src/sweetalert2.scss';
import Swal from 'sweetalert2';
import {User}  from 'src/app/model/User.model';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  users: User[];
  since:number;
  user: ResponseUser;
  dtResponsiveOptions: any = {};
  htmlRepositories : string;
  htmlDetails : string;
  nextPageUrl : string;
  public showMoreItens;
  constructor(
    private router: Router,
    private ajax: AjaxService,
    public util: UtilService,
    private auth: AuthService,
  ) { }

  ngOnInit() {
    this.user = this.auth.getUser();
    this.users = [];
    this.since =0;
    this.getUsers();
  }


  isCanvas(obj: HTMLCanvasElement | HTMLElement): obj is HTMLCanvasElement {
    return obj.tagName === 'CANVAS';
  } intervaloMax


  makeRepositorieHtmlLine(repositorieLine)
  {
    return `
    <tr>
      <td>${repositorieLine.id}</td>
      <td>${repositorieLine.name}</td>
      <td><a target="_blank" href="${repositorieLine.html_url}">Click to view Repositorie</a> </td>
    </tr>`;
  }

 async makeHtmlTableRepositories(user:User)
  {
    this.htmlRepositories='';
    let url =  `${API}user/${user.login}/repos`;

    await this.ajax.get(url)    
    .then(resp => {

      let htmlTr='' ;

     if(resp && resp.repositories)
       resp.repositories.map(rep => htmlTr += this.makeRepositorieHtmlLine(rep));
     else
       htmlTr= 'No have data for Repositories';

     this.htmlRepositories = `
     <fieldset>
       <legend>Repositories</legend>
       <table class="table">
         <thead">                                                                       
           <tr>       
             <th>Id</th>        
             <th>Name</th>                                                                        
             <th>Repositorie Url</th>                                                              
           </tr>                                                                         
         </thead>                                                                      
         <tbody>
           ${htmlTr}                                                               
         </tbody>                                                                      
      </table>
     </fieldset>`;
    
    }, err => {
      this.util.toasterShowError("Caution", "Error in Get Data");
    });
  
  }
  

makeHtmlTableDetail(user:User, detail)
{
  let html = `<table class="table">
    <thead">                                                                       
    <tr>       
      <th>Id</th>        
      <th>Profile</th>                                                                        
      <th>Created At</th>                                                              
    </tr>                                                                         
   </thead>                                                                      
    <tbody>
    <tr>
    <td >${user.id}</td>
    <td ><a target='_blank' href='${detail.html_url}'>Click to view profile</a></td>
    <td >${this.util.formatDateString(detail.created_at)}</td>
    </tr>                                                                
  </tbody>                                                                      
  </table>`;

  return html;
}

  async makeHtmlDetailTextUser(user:User){
     
    let url =  `${API}user/${user.login}/details`;

    await this.ajax.get(url)
    .then(resp => {
      
      if(resp && resp.details)
        this.htmlDetails = this.makeHtmlTableDetail(user,resp.details);
       else
        this.htmlDetails ='No have data form Details';

    }, err => {
      this.util.toasterShowError("Caution", "Error in Get Data");
    });
  }
  
  async openDetails(user:User)
  {
     await this.makeHtmlTableRepositories(user);
     await this.makeHtmlDetailTextUser(user);

     let html = `
     <div class="modal-body"> 
       <div class="table-responsive" style="width:100%;">														     
       ${this.htmlDetails}  
       ${this.htmlRepositories}
     </div>`;

     this.showModal(user,html);
    
  }

  showModal(user:User,html){
    Swal.fire({
      title: `${user.login} Details`,
      type: 'warning',
      html: html,
      width: '95%'
    }).then((result) => { });
  }

   
  clearUsers= () => this.users = [];
  
  bindUsers  (dataUsers)  {
    dataUsers.map((user) => {
      var bindUser = new User();
      bindUser.id = user.id;
      bindUser.login = user.login;
      this.users.push(bindUser);
    });
  }

  async getUsers() {

    var url = `${API}user/?since=${this.since}&pageSize=20`;

    await this.ajax.get(url)
      .then(resp => {

        this.clearUsers();
        this.showMoreItens = resp && resp.users;

        if(  this.showMoreItens)
        {
          this.bindUsers(resp.users);
          this.nextPageUrl = resp.next_page_url;
        }

      }, err => {
        this.util.toasterShowError("Caution", "Error in Get Data");
      });
  }

  async getMoreUsers() {

    var url = this.nextPageUrl;

    await this.ajax.get(url)
      .then(resp => {
        if(resp && resp.users){
          this.bindUsers(resp.users);
          this.nextPageUrl = resp.next_page_url;
        }
      }, err => {
        this.util.toasterShowError("Caution", "Error in Get Data");
      });
     
      setTimeout(() => {
      this.goToTheBottom();
     }, 500);
     
  }

  goToTheTop() {
  document.body.scrollTop = 0;
  document.documentElement.scrollTop = 0;
   }

   goToTheBottom() {
    document.body.scrollTop = document.body.offsetHeight;
    document.documentElement.scrollTop = document.body.offsetHeight;
     }
}