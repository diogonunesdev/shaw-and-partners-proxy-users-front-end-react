import{HttpInterceptor,HttpRequest,HttpHandler,HttpEvent} from '@angular/common/http'
import {Observable} from 'rxjs/Observable'
import{Injectable,Injector} from '@angular/core'
import { AuthService } from 'src/app/service/auth.service';

@Injectable()
export class AuthInterceptor implements HttpInterceptor{

  constructor(private injector: Injector){}

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>>{

    const loginService = this.injector.get(AuthService)
    if(loginService.isLoggedIn()){
      const authRequest =  null;
      /*const authRequest = request.clone(
            {setHeaders:{'Authorization': `Bearer ${loginService.user.access_token}`,
                        'tipoUser':`${loginService.user.quantuser}`}})*/
      return next.handle(authRequest)
    }else{

      return next.handle(request)
    }
  }

}
