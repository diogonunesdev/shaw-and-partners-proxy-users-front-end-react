import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ResponseUser } from 'src/app/model/User.model';
import { AuthService } from 'src/app/service/auth.service';
import { UtilService } from 'src/app/service/util.service';
import {User}  from 'src/app/model/User.model';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  email: string;
  senha: string;
  loggedUser : ResponseUser = new ResponseUser();
  user:User = new User();

  constructor(
    private router: Router,
    private util: UtilService,
    private auth: AuthService,
  ) { }

  ngOnInit() {
    this.email = '';
    this.senha = '';
  }

  login(){

    if(this.email == "shawandpartners" && this.senha =="123456"){
      this.user.id = "1";
      this.user.login = "Shaw And Partners";
      this.loggedUser.user =this.user;
      this.auth.setUser(this.loggedUser);
      this.router.navigate(['/home']);
    }
    else
    {
    this.util.toasterShowError("Atenção", "Usuário ou Senha inválidos");
    }
  }
}